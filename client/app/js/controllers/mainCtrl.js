angular.module("LoginApp")
    .controller("mainCtrl", function ($scope, $state) {

    const loginButtonName = "LOGIN";
    const accountButtonName = "ACCOUNT";
    const connectAdminButtonName = "CONNECT WITH ADMIN";

    $scope.loginName = loginButtonName;
    $scope.accountName = accountButtonName;
    $scope.connectName = connectAdminButtonName;

    $scope.homeButton = function () {
        $state.go('index.home');
    }
});
