angular.module("LoginApp").controller("messageCtrl", function ($scope, $http, $state) {

    const messageLogin = "Enter your email address and your password to sing in.";
    const messageErrorLogin = "Please, enter right email/password, or if you weren't be registered - connect with admin."

    $scope.dismiss = function () {
        $scope.$dismiss();
        $state.go('index.home');
    };

    $scope.messageLogin = messageLogin;

    $scope.inputErrorMessage = function (err) {
        if (angular.isDefined(err)) {
            if (err.required) {
                return "The field can't be empty"
            }
            if (err.email) {
                return "Enter the correct email"
            }
        }
    };

    // $scope.checkNewUser = function (newUser) {
    //     $http({
    //         method: "POST",
    //         url: "http://localhost:3000/api/login",
    //         data: newUser
    //     })
    //         .then(function successCallback(response) {
    //             $scope.dismiss();
    //         }, function errorCallback(error) {
    //             if (error.status === 401) {
    //                 $scope.messageLogin = false;
    //                 $scope.messageErrorLogin = messageErrorLogin;
    //             }
    //         });
    // };
});

