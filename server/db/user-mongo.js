const mongoose = require('mongoose');
const config = require('../config');
mongoose.connect(config.get('mongoose:uri'));

const passportLocalMongoose = require('passport-local-mongoose');

const db = mongoose.connection;
db.on('error', function () {
    return console.log(error);
});
db.once('open', function () {
    console.log('Connection established!');
});

const userSchema = mongoose.Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: String
});

userSchema.plugin(passportLocalMongoose);
const UserModel = mongoose.model('user', userSchema);
module.exports.UserModel = UserModel;




