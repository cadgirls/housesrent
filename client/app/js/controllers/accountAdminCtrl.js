angular.module("LoginApp")
    .controller("accountCtrl", function ($scope, $http, $state) {

        let userCurrentRole;
        let userId = '';
        let newAccommodation = {};

        $http({
            method: "GET",
            url: "http://localhost:3000/api/user"
        })
            .then(function successCallback(response) {
                userCurrentRole = response.data.role;
                userId = response.data._id;

                loadPageAccount(userCurrentRole, $state);
            }, function errorCallback(error) {
                if (error.status === 401) {
                    console.log(error)
                }
                if (error.status === 403) {
                    alert("You wasn't login! Please, login yourself. If you wasn't registered, you need to connection with admin.")
                    $state.go('index.home');
                }
            });

        function loadPageAccount(userRole, $state) {
            if (userRole === 'admin') {
                $state.go('index.account.admin');
                $http({
                    method: "GET",
                    url: "http://localhost:3000/api/users"
                })
                    .then(function (response) {
                        $scope.items = response.data;
                    })
            } else {
                $state.go('index.account.publisher');
            }
        };

        $scope.submitAccomodation = () => {
          if($scope.city && $scope.district && $scope.rooms && $scope.price) {
                newAccommodation = {
                    city: $scope.city,
                    district: $scope.district,
                    rooms: $scope.rooms,
                    price: $scope.price
                };

                $scope.city = '';
                $scope.district = '';
                $scope.rooms = '';
                $scope.price = '';
            }
        }
})
  .directive("fileread", [function () { //upload image file
  return {
    scope: {
      fileread: "="
    },
    link: function (scope, element, attributes) {
      element.bind("change", function (changeEvent) {
        scope.$apply(function () {
          scope.fileread = changeEvent.target.files[0];

          console.log(scope.fileread.name) //image obj

          //append image
          // let img = `<img src="../../img/' + scope.fileread.name + " alt="" class="img-responsive">`;
          // document.querySelector('.photoContainer').appendChild(img);

          // or all selected files:
          // scope.fileread = changeEvent.target.files;
        });
      });
    }
  }
}]);
