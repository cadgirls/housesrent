const app = angular.module("LoginApp", ['ui.router', 'ui.bootstrap']);

app.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('index', {
            templateUrl: '../views/templates/main.html',
            controller: 'mainCtrl'
        })

        .state('index.home',{
            url: '/',
           templateUrl: '../views/templates/advertisementBlocks.html',
            controller: 'advertisementsCtrl'
        })

        .state('index.login', {
            url: '/login',
            onEnter: function ($uibModal) {
                $uibModal.open({
                    templateUrl: '../views/templates/login.html',
                    controller: 'loginCtrl',
                });
            }
        })

        .state('index.connect', {
            url: '/connect',
            onEnter: function ($uibModal) {
                $uibModal.open({
                    templateUrl: '../views/templates/connect.html',
                    controller: 'connectCtrl',
                });
            }
        })

        .state('index.message', {
            url: '/message',
            onEnter: function ($uibModal) {
                $uibModal.open({
                    templateUrl: '../views/templates/message.html',
                    controller: 'messageCtrl',
                });
            }
        })

        .state('index.account', {
            url: '/account',
            controller: 'accountCtrl'
        })

        .state('index.account.admin',{
            url:'/admin',
            templateUrl: '../views/templates/accountAdmin.html',
            controller: 'accountCtrl'
        })

        .state('index.account.publisher',{
            url:'/publisher',
            templateUrl: '../views/templates/accountPublisher.html',
            controller: 'accountCtrl'
        })
});

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);
