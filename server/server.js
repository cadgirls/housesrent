const express = require('express');
const favicon = require('serve-favicon');
const app = express();
const userModel = require('./db/user-mongo').UserModel;
const config = require('./config');
const logger = require('morgan');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const path = require('path');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' })

app.use(favicon(__dirname + '/public/img/house.png'));
app.use(express.static('./public'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({secret: 'SECRET'}));
app.use(cookieParser());

app.use(session({
    secret: config.get('session:secret'),
    cookie: config.get('session:cookie'),
    resave: config.get('session:resave'),
    saveUninitialized: config.get('session:saveUninitialize')
}));

let passport = require('./models/passport').passport;

app.use(passport.initialize());
app.use(passport.session());
app.post('/api/login', passport.authenticate('local', {
    successRedirect: '/'
}));


app.all('/api/user', function (req, res, next) {
    req.isAuthenticated()
        ? next()
        : res.sendStatus(403);
});

app.post('/api/users', function (req, res) {
    let newuser = new userModel({
        email: req.body.email,
        password: req.body.password
    });

    res.send(newuser);
    newuser.save(function (err, data) {
        if (!data) {
            console.log('Error save.')
        }
        if (!err) {
            console.log({status: "saved", data: newuser});

        } else {
            console.log(err);
        }
    });
});

app.get('/api/users', function (req, res) {
    userModel.find(function (error, obj) {
        if (!obj) {
            console.log('Error found.')
        }
        if (!error) {
            res.send(obj);
        } else {
            console.log(error);
        }

    })
});

app.get('/api/user', function (req, res) {
    let user = req.user;
    console.log(user);
    userModel.findById(user._id, function (error, obj) {
        if (!obj) {
            console.log('Error found.')
        }
        if (!error) {
            res.send(obj);
        } else {
            console.log(error);
        }

    })
});

app.get('/api/users/:id', function (req, res) {
    userModel.findById(req.params.id, function (error, obj) {
        if (!obj) {
            console.log('Error found.')
        }
        if (!error) {
            res.send(obj);
            console.log({status: "found", data: obj})
        } else {
            console.log(error);
        }
    })
});

app.delete('/api/users/:id', function (req, res) {
    userModel.remove({
        _id: req.params.id
    }, function (err) {
        if (err)
            res.send(err);

        res.json({message: 'Successfully deleted'});
    });
});

app.put('/api/users/:id', function (req, res) {
    userModel.findById(req.params.id, function (err) {
        if (err)
            res.send(err);

        userModel.role = req.body.role;

        userModel.save(function (err) {
            if (err)
                res.send(err);

            res.json({message: 'User updated!'});
        });
    });
});

app.listen(config.get('port'), function () {
    console.log('Server started on port ' + config.get('port'));
});