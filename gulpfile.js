'use strict';
const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const gulpIf = require('gulp-if');
const del = require('del');
const url = require('gulp-css-url');
const htmlreplace = require('gulp-html-replace');

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

gulp.task('scss', function () {
    return gulp.src('./client/app/css/**.scss')
        .pipe(gulpIf(isDevelopment, sourcemaps.init()))
        .pipe(sass())
        .pipe(gulpIf(isDevelopment,sourcemaps.write()))
        .pipe(gulp.dest('./client/app/css/css'))
        .pipe(gulpIf(!isDevelopment, concat('main.css')))
        .pipe(gulp.dest('./server/public/css/css'));
});

/*gulp.task('html:replace', function() {
    gulp.src('./client/app/index.html')
        .pipe(htmlreplace({
            'css': './css/css/main.css',
        }))
        .pipe(gulp.dest('./server/public/'));
});*/

gulp.task('copy', function () {
   return gulp.src(['./client/app/**/*.*', '!client/app/css/**'])
       .pipe(gulp.dest('./server/public/'));
});

gulp.task('clean', function () {
    return del('./server/public');
});

gulp.task('build', ['scss','copy']);

gulp.task('default', ['clean'], function () {
    gulp.start('build')
});

gulp.watch('client/app/css/**.scss', ['scss']);
gulp.watch(['./client/app/**/*.*', '!client/app/css/**'], ['copy']);
//gulp.watch('./client/app/index.html', ['html:replace']);


