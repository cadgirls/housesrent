Guide to starting an app:
 1. Pull project
 2. Go to "task2" and run:
     1) npm install
     2) gulp
 3. Go to "task2/server"
 4. Run:
      1) npm install
      2) node server.js
 5. You can see in browser:
      1) Home page on http://localhost:3000/
      2) saved users in MongoDB on http://localhost:3000/api/users
      
      For access you may use: 
            email: name4@mail.com
            password: 123
            
      
 
